package com.example.limpin.stilltryingout;

import android.icu.text.DateFormat;
import android.icu.text.SimpleDateFormat;
import android.icu.util.Calendar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private boolean In_edit_mode =true;
    @Override

    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final Button btn_save = (Button)(findViewById(R.id.btn_save ) );
        final EditText tb_notes = (EditText)(findViewById(R.id.tb_notes) );
        tb_notes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tb_notes.setText("") ;
            }
        });
        btn_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EditText tb_titles = (EditText)(findViewById(R.id.tb_titles ));
                if (In_edit_mode)
                {
                    In_edit_mode =false;
                    btn_save.setText("Edit");
                    tb_titles.setEnabled(false);
                    tb_notes .setEnabled(false);
                    TextView tb_dateview = (TextView)(findViewById(R.id.tb_dateview) );
                    DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
                    String date = dateFormat.format(Calendar.getInstance().getTime());
                    tb_dateview.setText(date);

                }
                else
                {
                    In_edit_mode = true;
                    btn_save.setText("save");
                    tb_titles.setEnabled(true);
                    tb_notes.setEnabled(true);
                }


            }
        });


    }
}
