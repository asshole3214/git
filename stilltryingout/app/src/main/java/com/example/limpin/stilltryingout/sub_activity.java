package com.example.limpin.stilltryingout;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class sub_activity extends AppCompatActivity {
    private List<notes> thisnote= new ArrayList<notes>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sub_activity);
        thisnote.add(new notes("First note" , "blah blah black sheep" , new Date()));
        thisnote.add(new notes("second note" , "this is hard" , new Date()));
        thisnote.add(new notes("third note" , "like fucking hard" , new Date()));
        thisnote.add(new notes("fourth note" , "so hard" , new Date()));
        ListView noteslistview = (ListView)(findViewById(R.id.noteslistview ) );
        ArrayList <String> values = new ArrayList<String>();
       /* values.add ("note 1");
        values.add ("note 2");
        values.add ("note 3");
        values.add ("note 4");*/
        for (notes note  : thisnote)
        {
            values.add(note.getTitle());
        }

        ArrayAdapter <String>  adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1 , android.R.id.text1,values ) ;
        noteslistview.setAdapter(adapter);
    }
}
